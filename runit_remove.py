import glob
import json
import pathlib

working_path = str(pathlib.Path().absolute())
for name in glob.glob('BigData-workshop-[0-9].ipynb'):
    file_name = str(name)
    _file = working_path +"/" + file_name
    with open(_file ,'r') as f:
        lesson_file = json.loads(f.read(),strict=False)
        for idx,field in enumerate(lesson_file['cells']):
            if field['cell_type']=="code" and (str(field['source'])).find('RUNIT')!=-1:
                try:
                    del lesson_file['cells'][idx]
                except Exception as error:
                    print("Caught Errors:\t{}".format(error))
        _name,_ext = file_name.split('.')
        _f_name = _name+ "-lesson"+"."+_ext
        try:
            with open(working_path +"/"+_f_name, "w") as output:
                output.write(json.dumps(lesson_file))
        except Exception as error:
            print("Errors Writting to file: \t{}".format(error))