# runit_remove_script

Simple script to edit jupyter notebook files by deleting cells containing `RUNIT` keyword

Output files are named with `lesson` added to original name
